from tkinter import *
from math import *
from Calculadora import Calculadora
from fractions import Fraction

ventana = Tk()
ventana.title('CALCULADORA')
ventana.geometry('900x500')
ventana.configure(background = 'gray20')
calc = Calculadora()

#Color de los botones
botonNumero = ('lightblue')
botonOperacion = ('gray50')
botonEspecial = ('red')
botonOtro =('lightgreen')
botonNoFuncional = ('black')
botonMemoria = ('darkblue')
botonConversion1 = ('lightgray')
botonConversion2 = ('darkgray')

#Tamaño de los botones
ancho = 10
alto = 2

#Posición de los botones
columna1 = 15
columna2 = 100
columna3 = 185
columna4 = 270
columna5 = 355
columna7 = 525
columna8 = 610
columna9 = 695
columna10 = 780
fila1 = 450
fila2 = 400
fila3 = 350
fila4 = 300
fila5 = 250
fila6 = 200
fila7 = 150

#Botones Numéricos
boton1 = Button(ventana, bg = botonNumero, text = '1', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna1, y = fila2)
boton2 = Button(ventana, bg = botonNumero, text = '2', width = ancho, height = alto, command = lambda:calc.click(2)).place(x = columna2, y= fila2)
boton3 = Button(ventana, bg = botonNumero, text = '3', width = ancho, height = alto, command = lambda:calc.click(3)).place(x = columna3, y= fila2)
boton4 = Button(ventana, bg = botonNumero, text = '4', width = ancho, height = alto, command = lambda:calc.click(4)).place(x = columna1, y= fila3)
boton5 = Button(ventana, bg = botonNumero, text = '5', width = ancho, height = alto, command = lambda:calc.click(5)).place(x = columna2, y= fila3)
boton6 = Button(ventana, bg = botonNumero, text = '6', width = ancho, height = alto, command = lambda:calc.click(6)).place(x = columna3, y= fila3)
boton7 = Button(ventana, bg = botonNumero, text = '7', width = ancho, height = alto, command = lambda:calc.click(7)).place(x = columna1, y= fila4)
boton8 = Button(ventana, bg = botonNumero, text = '8', width = ancho, height = alto, command = lambda:calc.click(8)).place(x = columna2, y= fila4)
boton9 = Button(ventana, bg = botonNumero, text = '9', width = ancho, height = alto, command = lambda:calc.click(9)).place(x = columna3, y= fila4)
boton0 = Button(ventana, bg = botonNumero, text = '0', width = ancho, height = alto, command = lambda:calc.click(0)).place(x = columna1, y = fila1)
botonPi = Button(ventana, bg = botonNumero, text = 'π', width = ancho, height = alto, command = lambda:calc.click('pi')).place(x = columna2, y = fila1)
botonE = Button(ventana, bg = botonNumero, text = 'e', width = ancho, height = alto, command = lambda:calc.click('e')).place(x = columna3, y = fila1)

#Botones de operaciones
botonSum = Button(ventana, bg = botonOperacion, text = '+', width = ancho, height = alto, command = lambda:calc.click('+')).place(x = columna4, y = fila2)
botonRes = Button(ventana, bg = botonOperacion, text = '-', width = ancho, height = alto, command = lambda:calc.click('-')).place(x = columna5, y = fila2)
botonMult = Button(ventana, bg = botonOperacion, text = 'x', width = ancho, height = alto, command = lambda:calc.click('*')).place(x = columna4, y = fila3)
botonDiv = Button(ventana, bg = botonOperacion, text = '/', width = ancho, height = alto, command = lambda:calc.click('/')).place(x = columna5, y = fila3)

#Botones especiales
botonIgual = Button(ventana, bg = botonEspecial, text = '=', width = ancho, height = alto, command = calc.calcular).place(x = columna5, y = fila1)
botonC = Button(ventana, bg = botonEspecial, text = 'C', width = ancho, height = alto, command = calc.clear).place(x = columna5, y = fila5)

#Otros botones
botonPunto = Button(ventana, bg = botonOtro, text = '.', width = ancho, height = alto, command = lambda:calc.click('.')).place(x = columna4, y = fila1)
botonParIzq = Button(ventana, bg = botonOtro, text = '(', width = ancho, height = alto, command = lambda:calc.click('(')).place(x = columna4, y = fila4)
botonParDer = Button(ventana, bg = botonOtro, text = ')', width = ancho, height = alto, command = lambda:calc.click(')')).place(x = columna5, y = fila4)
botonRaiz = Button(ventana, bg = botonOtro, text = '√', width = ancho, height = alto, command = lambda:calc.click('sqrt')).place(x = columna1, y = fila5)
botonPot = Button(ventana, bg = botonOtro, text = '^', width = ancho, height = alto, command = lambda:calc.click('**')).place(x = columna2, y = fila5)
botonLog = Button(ventana, bg = botonOtro, text = 'log', width = ancho, height = alto, command = lambda:calc.click('log')).place(x = columna3, y = fila5)
botonFrac = Button(ventana, bg = botonOtro, text = 'frac', width = ancho, height = alto, command = lambda:calc.click('frac')).place(x = columna4, y = fila5)

#Botones de memoria
botonMemo1 = Button(ventana, bg = botonMemoria, text = 'Guardar 1', width = ancho, height = alto, command = lambda:calc.click('G1')).place(x = columna1, y = fila6)
botonMemo1 = Button(ventana, bg = botonMemoria, text = 'Guardar 2', width = ancho, height = alto, command = lambda:calc.click('G2')).place(x = columna2, y = fila6)
botonMemo1 = Button(ventana, bg = botonMemoria, text = 'Guardar 3', width = ancho, height = alto, command = lambda:calc.click('G3')).place(x = columna3, y = fila6)
botonMemo1 = Button(ventana, bg = botonMemoria, text = 'Guardar 4', width = ancho, height = alto, command = lambda:calc.click('G4')).place(x = columna4, y = fila6)
botonMemo1 = Button(ventana, bg = botonMemoria, text = 'Guardar 5', width = ancho, height = alto, command = lambda:calc.click('G5')).place(x = columna5, y = fila6)
botonUsarMemo1 = Button(ventana, bg = botonMemoria, text = 'Usar 1', width = ancho, height = alto, command = lambda:calc.click('U1')).place(x = columna1, y = fila7)
botonUsarMemo1 = Button(ventana, bg = botonMemoria, text = 'Usar 2', width = ancho, height = alto, command = lambda:calc.click('U2')).place(x = columna2, y = fila7)
botonUsarMemo1 = Button(ventana, bg = botonMemoria, text = 'Usar 3', width = ancho, height = alto, command = lambda:calc.click('U3')).place(x = columna3, y = fila7)
botonUsarMemo1 = Button(ventana, bg = botonMemoria, text = 'Usar 4', width = ancho, height = alto, command = lambda:calc.click('U4')).place(x = columna4, y = fila7)
botonUsarMemo1 = Button(ventana, bg = botonMemoria, text = 'Usar 5', width = ancho, height = alto, command = lambda:calc.click('U5')).place(x = columna5, y = fila7)

#Botones de conversión
botonVol1 = Button(ventana, bg = botonConversion1, text = 'cm³', width = ancho, height = alto, command = lambda:calc.conver('cm3', 'volumen')).place(x = columna7, y = fila6)
botonVol2 = Button(ventana, bg = botonConversion1, text = 'm³', width = ancho, height = alto, command = lambda:calc.conver('m3', 'volumen')).place(x = columna8, y = fila6)
botonVol3 = Button(ventana, bg = botonConversion1, text = 'L', width = ancho, height = alto, command = lambda:calc.conver('L', 'volumen')).place(x = columna9, y = fila6)
botonVol4 = Button(ventana, bg = botonConversion1, text = 'gal', width = ancho, height = alto, command = lambda:calc.conver('gal', 'volumen')).place(x = columna10, y = fila6)
botonLon1 = Button(ventana, bg = botonConversion2, text = 'mm', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna7, y = fila5)
botonLon2 = Button(ventana, bg = botonConversion2, text = 'cm', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna8, y = fila5)
botonLon3 = Button(ventana, bg = botonConversion2, text = 'm', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna9, y = fila5)
botonLon4 = Button(ventana, bg = botonConversion2, text = 'km', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna10, y = fila5)
botonTemp1 = Button(ventana, bg = botonConversion1, text = '°C', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna7, y = fila4)
botonTemp2 = Button(ventana, bg = botonConversion1, text = '°F', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna8, y = fila4)
botonTemp3 = Button(ventana, bg = botonConversion1, text = 'K', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna9, y = fila4)
botonPeso1 = Button(ventana, bg = botonConversion2, text = 'g', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna7, y = fila3)
botonPeso2 = Button(ventana, bg = botonConversion2, text = 'Kg', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna8, y = fila3)
botonPeso3 = Button(ventana, bg = botonConversion2, text = 't', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna9, y = fila3)
botonPeso4 = Button(ventana, bg = botonConversion2, text = 'lb', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna10, y = fila3)
botonPre1 = Button(ventana, bg = botonConversion1, text = 'Pa', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna7, y = fila2)
botonPre2 = Button(ventana, bg = botonConversion1, text = 'atm', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna8, y = fila2)
botonPre3 = Button(ventana, bg = botonConversion1, text = 'mm Hg', width = ancho, height = alto, command = lambda:calc.click(1)).place(x = columna9, y = fila2)

entrada = Entry(ventana, font = ('arial', 20, 'bold'), width = 25, textvariable = calc.pantalla, bd = 15, justify = 'right').place(x=15,y=60)
