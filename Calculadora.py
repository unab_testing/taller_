from tkinter import *
from math import *
from fractions import Fraction

class Calculadora(object):

    def __init__(self):
        self.memoria1 = ''
        self.memoria2 = ''
        self.memoria3 = ''
        self.memoria4 = ''
        self.memoria5 = ''

        self.operador = ''
        self.mostrador = ''
        self.pantalla = StringVar()

        self.unidad = ''
        self.tipo = ''
        self.valor = 0

    def click(self, num):
        if num == 'G1':
            self.memoria1 = '(' + self.operador + ')'
        elif  num == 'G2':
            self.memoria2 = '(' + self.operador + ')'
        elif  num == 'G3':
            self.memoria3 = '(' + self.operador + ')'
        elif  num == 'G4':
            self.memoria4 = '(' + self.operador + ')'
        elif  num == 'G5':
            self.memoria5 = '(' + self.operador + ')'
        elif num == 'U1':
            self.operador = self.operador + self.memoria1
            self.mostrador = self.mostrador + 'Mem1'
            self.pantalla.set(self.mostrador)
        elif num == 'U2':
            self.operador = self.operador + self.memoria2
            self.mostrador = self.mostrador + 'Mem2'
            self.pantalla.set(self.mostrador)
        elif num == 'U3':
            self.operador = self.operador + self.memoria3
            self.mostrador = self.mostrador + 'Mem3'
            self.pantalla.set(self.mostrador)
        elif num == 'U4':
            self.operador = self.operador + self.memoria4
            self.mostrador = self.mostrador + 'Mem4'
            self.pantalla.set(self.mostrador)
        elif num == 'U5':
            self.operador = self.operador + self.memoria5
            self.mostrador = self.mostrador + 'Mem5'
            self.pantalla.set(self.mostrador)
        elif num == '*':
            self.operador = self.operador + '*'
            self.mostrador = self.mostrador + 'x'
            self.pantalla.set(self.mostrador)
        elif num == 'pi':
            self.operador = self.operador + 'pi'
            self.mostrador = self.mostrador + 'π'
            self.pantalla.set(self.mostrador)
        elif num == 'sqrt':
            self.operador = self.operador + 'sqrt('
            self.mostrador = self.mostrador + '√('
            self.pantalla.set(self.mostrador)
        elif num == 'log':
            self.operador = self.operador + 'log('
            self.mostrador = self.mostrador + 'log('
            self.pantalla.set(self.mostrador)
        elif num == '**':
            self.operador = self.operador + '**'
            self.mostrador = self.mostrador + '^'
            self.pantalla.set(self.mostrador)
        elif num == 'frac':
            self.operador = self.operador + 'Fraction('
            self.mostrador = self.mostrador + 'frac('
            self.pantalla.set(self.mostrador)
        else:
            self.operador = self.operador + str(num)
            self.mostrador = self.mostrador + str(num)
            self.pantalla.set(self.mostrador)

    def converVolumen(self, n, u1, u2):
        if u1 == 'cm3' and u2 == 'cm3':
            self.valor = n
        elif u1 == 'cm3' and u2 == 'm3':
            self.valor = n / 1000000
            self.unidad = 'm3'
        elif u1 == 'cm3' and u2 == 'L':
            self.valor = n / 1000
            self.unidad = 'L'
        elif ul == 'cm3' and u2 == 'gal':
            self.valor = n / 3785.412
            self.unidad = 'gal'
        elif u1 == 'm3' and u2 == 'cm3':
            self.valor = n * 1000000
            self.unidad = 'cm3'
        elif u1 == 'm3' and u2 == 'm3':
            self.valor = n
        elif u1 == 'm3' and u2 == 'L':
            self.valor = n * 1000
            self.unidad = 'L'
        elif ul == 'm3' and u2 == 'gal':
            self.valor = n * 264.172
            self.unidad = 'gal'
        elif u1 == 'L' and u2 == 'cm3':
            self.valor = n * 1000
            self.unidad = 'cm3'
        elif u1 == 'L' and u2 == 'm3':
            self.valor = n / 1000
            self.unidad = 'm3'
        elif u1 == 'L' and u2 == 'L':
            self.valor = n
        elif ul == 'L' and u2 == 'gal':
            self.valor = n / 3.785
            self.unidad = 'gal'
        elif u1 == 'gal' and u2 == 'cm3':
            self.valor = n * 3785.412
            self.unidad = 'cm3'
        elif u1 == 'gal' and u2 == 'm3':
            self.valor = n / 264.172
            self.unidad = 'm3'
        elif u1 == 'gal' and u2 == 'L':
            self.valor = n * 3.785
            self.unidad = 'L'
        elif ul == 'gal' and u2 == 'gal':
            self.valor = n

    def conver(self, u, t):
        if self.unidad == '' and self.tipo == '':
            self.unidad = u
            self.tipo = t
            self.valor = self.operador
        elif self.tipo == 'volumen':
            self.converVolumen(self.operador, self.unidad, u)
        self.pantalla.set(self.valor + self.unidad)

    def clear(self):
        self.operador = ''
        self.mostrador = ''
        self.pantalla.set('0')

    def calcular(self):
        try:
            operacion = str(eval(self.operador))
        except:
            self.clear()
            operacion = 'ERROR'
        self.pantalla.set(operacion)
        